export default {
  namespaced: true,
  state: {
    selected: 'shipping',
    items: [
      {
        title: 'Shipping',
        to: 'shipping',
        filled: false
      },
      {
        title: 'Billing',
        to: 'billing',
        filled: false
      },
      {
        title: 'Payment',
        to: 'payment',
        filled: false
      },
      {
        title: 'End',
        to: 'end',
        filled: false
      }
    ]
  },
  getters: {
    items: state => state.items,
    selected: state => state.selected
  },
  mutations: {
    updateFilled (state, filled) {
      const navLink = state.items.find(item => item.to === state.selected)
      navLink.filled = filled
    },
    updateSelected (state, val) {
      state.selected = val
    }
  }
}
