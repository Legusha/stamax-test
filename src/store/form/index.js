import navigate from './navigate'

export default {
  namespaced: true,
  state: {
    errors: [],
    data: {}

  },
  getters: {
    errors: state => state.errors,
    data: state => state.data
  },
  mutations: {
    setData (state, { key, data }) {
      state.data[key] = data
    },
    setErrors (state, errors) {
      state.errors = errors
    }
  },
  modules: {
    navigate
  }
}
