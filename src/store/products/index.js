export default {
  namespaced: true,
  state: {
    // taxes are assumed as a percentage
    taxes: 12,
    list: [
      {
        name: 'The Chelsea boot',
        colors: ['black'],
        img: 'https://b3h2.scene7.com/is/image/BedBathandBeyond/17892785172843m?$imagePLP$&wid=256&hei=256',
        price: 235
      },
      {
        name: 'The Twill Snack Backpack',
        colors: ['Reverse Denim + Brown Leather'],
        img: 'https://summitsports.scene7.com/is/image/SummitSports/531406_531406_1?$256$',
        price: 65
      },
      {
        name: 'The Twill Zip Tote',
        colors: ['Reverse Denim + Brown Leather'],
        img: 'http://lghttp.18965.nexcesscdn.net/8092B3/magento/media/catalog/product/cache/1/image/256x/9df78eab33525d08d6e5fb8d27136e95/1/1/11099-054-intruder-boot-salt-boulder_s18_preview.jpeg',
        price: 48
      }
    ]
  },
  getters: {
    list: state => state.list,
    taxes: state => state.taxes
  },
  mutations: {
  }
}
