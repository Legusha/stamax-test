import Vue from 'vue'
import Vuex from 'vuex'

import form from './form'
import products from './products'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    form,
    products
  }
})
