export const shipRule = {
  name: ['isEmptyString'],
  customField: ['isEmpty'],
  street: ['isEmptyString', 'isEmpty'],
  city: ['isEmpty'],
  zip: ['isEmpty']
}
export const paymentRule = {
  name: ['isEmpty'],
  number: ['isEmpty'],
  expire: ['isEmpty'],
  security: ['isEmpty']
}
