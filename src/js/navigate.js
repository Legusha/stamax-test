import store from '@/store'

class NavIterator {
  constructor ({ selected = 'shipping', items = [] }) {
    if (typeof NavIterator.instance === 'object') {
      return NavIterator.instance
    }
    this.index = 0
    this.list = items
    this.selected = selected
    NavIterator.instance = this
    return this
  }

  next () {
    if (this.hasNext()) {
      this.index++
      const nextElem = this.list[this.index]
      this.updateSelected(nextElem)
      return nextElem
    }
  }
  back () {
    if (this.hasBack()) {
      this.index--
      this.updateSelected(this.list[this.index])
      return this.list[this.index]
    }
  }
  goTo (value) {
    const indexLink = this.list.findIndex(item => item.to === value)
    if (indexLink >= 0) {
      this.index = indexLink
      this.updateSelected(this.list[this.index])
    }
  }
  getSelected () {
    return this.list[this.index]
  }
  hasNext () {
    return (this.index + 1) !== this.list.length
  }
  hasBack () {
    return this.index !== 0
  }
  updateFilled (filled) {
    store.commit('form/navigate/updateFilled', filled)
    return this
  }
  writeData (data) {
    store.commit('form/setData', { data, key: this.selected })
    return this
  }
  updateSelected (el) {
    store.commit('form/navigate/updateSelected', el.to)
  }
}
export default new NavIterator(store.state.form.navigate)
