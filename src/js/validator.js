import { shipRule, paymentRule } from './rules'
import Vue from 'vue'
// import store from '@/store'
function pay ({ data, errors }) {
  return new PaymentRules(data, errors, paymentRule)
}
function ship ({ data, errors }) {
  return new ShippingRules(data, errors, shipRule)
}
class GeneralMethods {
  constructor (inputsList, errors = {}) {
    this.data = inputsList
    this.errors = errors
  }
  isString (val) {
    if (typeof val === 'string') return true
    return false
  }
  isEmpty (val) {
    if (val && val.length > 0) return true
    return false
  }
  isEmptyString (val) {
    if (this.isString(val) && this.isEmpty(val)) return true
    return false
  }
  findRules (prop) {
    return this.data.hasOwnProperty(prop) && this.rules.hasOwnProperty(prop)
  }
  checkByProp (prop) {
    if (this.findRules(prop)) {
      if (Array.isArray(this.rules[prop])) {
        return this.checkRulesByArray(this.isString(this.data[prop]) ? this.data[prop] : this.data[prop].text, this.rules[prop], prop)
      }
      if (this.isEmptyString(this.rules[prop])) {
        this.checkOnceRule(this.data[prop], this.rules[prop])
        return true
      }
      throw new Error('Check typing rules, or string with one rule not be empty')
    }
    // throw new Error(`Handler or property ${prop} could not be found`)
  }
  findError (value, err, r) {
    if (this[r]) {
      if (!this[r](value)) {
        err.push(r)
      }
    }
    return err
  }
  writeErrors (errors, prop) {
    Vue.set(this.errors, prop, errors)
  }
  checkRulesByArray (value, rulesArr, prop) {
    const errors = rulesArr.reduce(this.findError.bind(this, value), [])
    this.writeErrors(errors, prop)
    return errors
  }
  checkOnceRule (value, rulesStr) {
  }
  checkAll () {
    if (this.data) {
      for (let key in this.data) {
        this.checkByProp(key)
      }
    }
  }
  hasErrors () {
    this.checkAll()
    for (let key in this.errors) {
      if (this.errors[key].length > 0) {
        return true
      }
    }
    return false
  }
}
class PaymentRules extends GeneralMethods {
  constructor (inputsList, errors, rules = {}) {
    super(inputsList, errors)
    this.rules = rules
  }
}
class ShippingRules extends GeneralMethods {
  constructor (inputsList, errors, rules = {}) {
    super(inputsList, errors)
    this.rules = rules
  }
}
export default function selectRules (v = '', data) {
  return v === 'payment' ? pay.bind(null, data) : ship.bind(null, data)
}

// const Validator = selectRules('shipping', { data: { name: ['isEmpty'], street: 'isEmpty' }, errors: [] })
// const shipping = new Validator()

// console.assert(shipping.checkByProp('name'), 'Rules or data prop is not found')
// console.assert(shipping.checkByProp('street'), 'Rules or data prop is not found')
